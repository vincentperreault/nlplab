classdef Proximal
    %Library of usual proximal operators and associated functions
    
    methods (Static)
        
        function value = RectangleIndicator(lowerbounds, upperbounds, argument)
            %          n
            % Soit C = x [lowerbounds , upperbounds ]
            %         i=1            i             i
            %
            % val = X (arg)
            %        C
            value = 0;
            if any(argument < lowerbounds) || any(argument > upperbounds)
                value = Inf;
            end
        end
        
        function res = ProxRectangleIndicator(lowerbounds, upperbounds, argument)
            % res = prox       (arg)
            %          gamma X
            %                 C
            res = argument;
            brokenbounds = res < lowerbounds;
            res(brokenbounds) = lowerbounds(brokenbounds);
            brokenbounds = res > upperbounds;
            res(brokenbounds) = upperbounds(brokenbounds);
        end
        
        function res = ProxConjRectangleIndicator(gamma, lowerbounds, upperbounds, argument)
            % res = prox                  (arg)
            %          gamma (lambda X )*
            %                         C
            res = zeros(size(argument));
            brokenbounds = argument < gamma * lowerbounds;
            res(brokenbounds) = argument(brokenbounds) - gamma * lowerbounds(brokenbounds);
            brokenbounds = argument > gamma * upperbounds;
            res(brokenbounds) = argument(brokenbounds) - gamma * upperbounds(brokenbounds);
        end
        
        function res = ProxL1(gamma, argument)
            % res = prox           (arg)
            %          gamma ||.||
            %                     1
            res = zeros(size(argument));
            greaterThan = argument >= gamma;
            lowerThan = argument < -gamma;
            res(greaterThan) = argument(greaterThan) - gamma;
            res(lowerThan) = argument(lowerThan) + gamma;
        end
        
        function res = ProxConjL1(lambda, argument)
            % res = prox                      (arg)
            %          gamma (lambda ||.|| )*
            %                             1
            res = argument;
            res(argument > lambda) = lambda;
            res(argument < -lambda) = -lambda;
        end
        
        function res = ProxL2(gamma, argument)
            % res = prox            (arg)
            %                     2
            %          gamma ||.||
            %                     2
            res = argument / (2*gamma + 1);
        end
        
        function res = ProxConjL2(gamma, lambda, argument)
            % res = prox            (arg)
            %                             2 
            %          gamma (lambda ||.|| )*
            %                             2
            res = argument / (gamma/(2*lambda) + 1);
        end
        
    end
    
end

