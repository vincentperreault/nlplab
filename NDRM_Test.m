%% Initialisation

close all;
clear all;
clc;

global ROOTDIR
ROOTDIR = fullfile(getenv('HOME'), 'projet');
DATADIR = 'projData';
MPDIR = fullfile(DATADIR, 'projMatrices');
addpath(genpath(fullfile(ROOTDIR, 'Objets')))
addpath(genpath(fullfile(ROOTDIR, 'scriptsreconstruction')))
addpath(genpath(fullfile(ROOTDIR, 'sparse')))
addpath(genpath(fullfile(ROOTDIR, 'NLPLab')))

%% G�n�ration ou lecture des donn�es XCAT
[Fant, Sino] = GenerationDonnees2D(DATADIR);

GeoObj = 'Circ';
Srie_Fant = Fant.creationSerie(DATADIR, 'GeoObj', GeoObj);

%% Cr�ation de la matrice de projection
nRayons = 3;
SparseRep = 'ICCS';
SparseMode = 'MatVec';
[~, maxThreads] = nCPUs();

Thetas = Sino.GeoP.nProjections;
Rhos = round(Srie_Fant.GeoS.Rows * Srie_Fant.GeoS.Columns / ...
    Thetas);
Srie_P_Fant = Srie_Fant.cvPol(Rhos, Thetas);

GeoM_P = GeoM_Factory.create(Srie_P_Fant.GeoS, Sino, ...
    'nRayons', nRayons);

[s, message, ~] = mkdir(MPDIR);

MP_P = MP_Pol(containers.Map({'GeoM', 'nThreads', 'SparseRep', ...
    'SparseMode', 'MatDir'}, ...
    {GeoM_P, maxThreads, SparseRep, SparseMode, MPDIR}));

%Calcul de la norme
normPtP = 2282.685;%0;
% Factor = cell(1,1);
% nBlocks = MP_P.GeoM.nBlocks;
% objBlkSize = MP_P.GeoM.objBlkSize;
% sinoBlkSize = MP_P.GeoM.sinoBlkSize;
% 
% MatPFile = fullfile(MP_P.MatDir, MP_P.Fichier);
% load(MatPFile, 'IlIcV')
% Factor{1} = BCirc(containers.Map({'Blocks', 'BlkRows', 'BlkCols', ...
%     'Val'}, ...
%     {nBlocks, sinoBlkSize, objBlkSize, ...
%     sparse(IlIcV(:, 1), IlIcV(:, 2), IlIcV(:, 3), sinoBlkSize, ...
%     objBlkSize * nBlocks)}));
% clear IlIcV
% 
% nThreads = MP_P.nThreads;
% Granul = ceil(physicalMemory() / 8 / 4 / objBlkSize^2);
% for idx = 1:Granul:nBlocks
%     iDBlks = (idx:min(idx+Granul-1, nBlocks));
%     DBlocks = DiagBlksFTF(Factor, iDBlks, nThreads);
%     for iBl = 1:length(iDBlks)
%         eigenValues = eig(DBlocks{iBl});
%         maxEigen = max(abs(eigenValues));
%         if maxEigen > normPtP
%             normPtP = maxEigen;
%         end
%     end
% end
% clear Factor

%% Reconstruction par FBP, en coordonn�es polaires

FBP_P = FBP(containers.Map({'MatP', 'GeoS', 'Sino'}, ...
    {MP_P, Srie_P_Fant.GeoS, Sino}));
recFBP_P = FBP_P.reconst();

% f = plot(recFBP_P.cvCart(Fant.nbPixels, Fant.nbPixels));

%% Paramètres des tests

penalLs = 1;%[1, 2]

lambdaPenalL1s = 1e-3;%[1e-4 5e-4 1e-3 5e-3 1e-2 5e-2];

lambdaPenalL2s = 5e-2;%[5e-3 1e-2 5e-2 1e-1 5e-1 1];
lambdaRelPrecL2s = 1;%[1e3 1e2 1e1 1 1e-1];

compareNoPrec = false;

inits = {zeros(262160, 1)};%, .2*ones(262160, 1), recFBP_P.Val(:)};
inits_lbls = {'0.0', '0.2', 'FBP'};

relaxParams = 1;%[0.85, 1, 1.15];
relPrimStpSzParams = .99;%[0.5, 0.75, 0.9, 0.99];

versions = 'B';%{'Yan'};%{'A', 'B', 'Yan'};

rVarTol = 1e-4;
rObjTol = 1e-10;

timeLimit = 20*60;
iterLimit = 1;

%% Préparation des tests
cd('~/Desktop/TestData');
if numel(dir('Fantome.png')) == 0
    f = plot(Srie_Fant);
    print(f, 'Fantome', '-dpng');
    close(f);
end
cd(ROOTDIR);

lambdaPenals = {lambdaPenalL1s, lambdaPenalL2s};
lambdaRelPrecs = {1, lambdaRelPrecL2s};
plotIterMax = [1 round(sqrt(10)), 10 round(sqrt(10)*10), 100, ...
    round(sqrt(10)*100), 1000, round(sqrt(10)*1000), 10000, ...
    round(sqrt(10)*1000),  100000, round(sqrt(10)*100000), 1000000, iterLimit];

Adeq = Linear_Gauss_noPond(containers.Map('MPrj', MP_P));

%% Tests
for penalL = penalLs
    for lambdaPenal = lambdaPenals{penalL}
        %% Construction du crit�re polaire

        if penalL == 1
            PenalGradObj = PenalGradObj_L1(containers.Map({'GeoS', 'lambda'},...
                {Srie_P_Fant.GeoS, lambdaPenal}));
        else
            PenalGradObj = PenalGradObj_L2(containers.Map({'GeoS', 'lambda'},...
                {Srie_P_Fant.GeoS, lambdaPenal}));
        end
        crit = Critere(containers.Map('J', {Adeq, PenalGradObj}));

        if compareNoPrec
            Pcnds = cell(1, numel(lambdaRelPrecs{penalL}) + 1);
            Pcnds{1} = '';
            Pcnds_lbls = cell(1, numel(lambdaRelPrecs{penalL}) + 1);
            Pcnds_lbls{1} = 'Aucun';
        else
            Pcnds = cell(1, numel(lambdaRelPrecs{penalL}));
            Pcnds_lbls = cell(1, numel(lambdaRelPrecs{penalL}));
        end
        
        for idx = 1:numel(lambdaRelPrecs{penalL})
            Pcnds_lbls{idx+compareNoPrec} = ['LmbdRel-' num2str(lambdaRelPrecs{penalL}(idx))];
            if lambdaRelPrecs{penalL}(idx) == 1
                Pcnds{idx+compareNoPrec} = Precond_Factory.create('DiagF', 'crit', crit);
            else
                PenalGradObj_prec = PenalGradObj_L2(containers.Map(...
                    {'GeoS', 'lambda'},...
                    {Srie_P_Fant.GeoS, lambdaRelPrecs{penalL}(idx)*lambdaPenal}));
                crit_prec = Critere(containers.Map('J', {Adeq, PenalGradObj_prec}));
                Pcnds{idx+compareNoPrec} = Precond_Factory.create('DiagF', 'crit', crit_prec);
            end
        end

        %% Tests <<
        for PcndIdx = 1:numel(Pcnds)
            %% Calcul de bornes des normes matricielles
            
            %Bound on norm of first difference matrix
            if penalL == 1
                sqNormD = 4*((Thetas/(2*pi)).^2 + Rhos.^2);
            else
                sqNormD = 4*(2*(Thetas/(2*pi)).^2 + Rhos);
            end
            
            %Bound on norm of C
            if strcmpi(Pcnds{PcndIdx}, '')
                normC = 1;
                if penalL == 1
                    normCHC = normPtP;
                else
                    normCHC = 0;
                    precDiagF = ones(262160);
                end
            else
                precDiagF = Pcnds{PcndIdx}.Mdiag(:);
                normC = max(abs(precDiagF));
                normCHC = 0;
            end
            
            %Bound on norm of Hessian
            if normCHC == 0
                nBlocks = MP_P.GeoM.nBlocks;
                objBlkSize = MP_P.GeoM.objBlkSize;
                [Factors, nThreads] = crit.NormMatFactors();
                Granul = ceil(physicalMemory() / 8 / 4 / objBlkSize^2);
                for idx = 1:Granul:nBlocks
                    iDBlks = (idx:min(idx+Granul-1, nBlocks));
                    DBlocks = DiagBlksFTF(Factors, iDBlks, nThreads);
                    for iBl = 1:length(iDBlks)
                        delta = diag(precDiagF(((idx + iBl - 2)*objBlkSize+1):((idx + iBl - 1)*objBlkSize)));
                        eigenValues = eig(delta * DBlocks{iBl} * delta);
                        maxEigen = max(abs(eigenValues));
                        if maxEigen > normCHC
                            normCHC = maxEigen;
                        end
                    end
                end
                clear Factor
            end
            
            %% Tests <<<
            for initIdx = 1:numel(inits)
                for version = versions
                    %% Exceptions version Yan
                    if strcmpi(version, 'Yan')
                        currRelaxParams = 1;
                        currRelPrimStpSzParams = (1-1e-8);
                    else
                        currRelaxParams = relaxParams;
                        currRelPrimStpSzParams = relPrimStpSzParams;
                    end
                    
                    %% Tests <<<<<
                    for relaxParam = currRelaxParams
                        for relPrimStpSzParam = currRelPrimStpSzParams
                            %% Si déjà fait, le sauter
                            if strcmpi(version, 'Yan')
                                filename = ['penL' num2str(penalL) '_lmbd' num2str(lambdaPenal)  '_prec' Pcnds_lbls{PcndIdx} '_init' inits_lbls{initIdx} '_vrsnYan'];
                            else
                                filename = ['penL' num2str(penalL) '_lmbd' num2str(lambdaPenal)  '_prec' Pcnds_lbls{PcndIdx} '_init' inits_lbls{initIdx} '_vrsn' version '_rlx' num2str(relaxParam) '_rpss' num2str(relPrimStpSzParam)];
                            end
                            cd('~/Desktop/TestData');
                            if numel(dir([filename '.txt'])) > 0
                                cd(ROOTDIR);
                                %continue;
                            end
                            cd(ROOTDIR);
                            
                            %% Paramètres PDS
                            
                            primMaxStpSz = 2 * min(2 - relaxParam, 1) / (2 * normCHC);
                            primStpSz = relPrimStpSzParam * primMaxStpSz;
                            if penalL == 1
                                if strcmpi(version, 'Yan')
                                    dualStpSz = (1-1e-8)/(primStpSz * normC.^2 * (1 + sqNormD));
                                else
                                    dualStpSz = (1-1e-8)*(1/primStpSz - 1/primMaxStpSz) / (normC.^2 * (1 + sqNormD));
                                end
                            else
                                if strcmpi(version, 'Yan')
                                    dualStpSz = (1-1e-8)/(primStpSz * normC.^2);
                                else
                                    dualStpSz = (1-1e-8)*(1/primStpSz - 1/primMaxStpSz) / (normC.^2);
                                end
                            end

                            %% Reconstruction PDS
                            
                            if strcmpi(Pcnds{PcndIdx}, '')
                                ndrm = model.NonDiffRecNoPrecModel('NDRM', crit, Sino, Srie_P_Fant.GeoS, inits{initIdx}, 0);
                            else
                                ndrm = model.NonDiffRecModel('NDRM', crit, Pcnds{PcndIdx}, Sino, Srie_P_Fant.GeoS, inits{initIdx}, 0);
                            end
                            
                            if strcmpi(version, 'Yan')
                                PDS = solvers.YanPDSSolver('Yan', ndrm, rVarTol, rObjTol, plotIterMax(end), timeLimit, primStpSz, dualStpSz, 2, plotIterMax(1:end-1));
                            else
                                PDS = solvers.CondatPDSSolver('Condat', ndrm, rVarTol, rObjTol, plotIterMax(end), timeLimit, relaxParam, primStpSz, dualStpSz, version, 2, plotIterMax(1:end-1));
                            end

                            PDS.solve();

                            %% Affichage/enregistrement des résultats

                            iters = PDS.logger.Iter;
                            varDiffs = PDS.logger.varDiff;
                            oldVarDiffs = PDS.logger.oldVarDiff;
                            objs = PDS.logger.Obj;
                            feass = PDS.logger.Feas;
                            xs = PDS.logger.Screenshots;

                            cd('~/Desktop/TestData');
                            mu = inits{initIdx};
                            resL1 = Srie_Pol(containers.Map({'GeoS', 'GPhy', 'Patient', 'Study', ...
                                        'SeriesDescription', 'Historique', 'Val'}, ...
                                        {Srie_P_Fant.GeoS, GPhy_AttRX(containers.Map('Unites', 'cm-1')), ...
                                        Sino.Patient, Sino.Study, 'Initial', {'Initial'}, ...
                                        reshape(mu, Srie_P_Fant.GeoS.Rhos, Srie_P_Fant.GeoS.Slices, ...
                                        Srie_P_Fant.GeoS.Thetas)}));
                            f = plot(resL1.cvCart(Fant.nbPixels, Fant.nbPixels));
                            print(f, [filename '_iter' num2str(0)], '-dpng');
                            close(f);

                            for i = 1:size(xs,2)
                                if strcmpi(Pcnds{PcndIdx}, '')
                                    mu = xs(1:end, i);
                                else
                                    mu = real(Pcnds{PcndIdx}.Direct(xs(1:end, i)));
                                end
                                resL1 = Srie_Pol(containers.Map({'GeoS', 'GPhy', ...
                                    'Patient', 'Study', 'SeriesDescription', 'Historique', ...
                                    'Val'}, ...
                                    {Srie_P_Fant.GeoS, GPhy_AttRX(containers.Map('Unites', 'cm-1')), ...
                                    Sino.Patient, Sino.Study, ...
                                    ['Condat, coord. cyl., iter ' num2str(plotIterMax(i))], ...
                                    {''}, ...
                                    reshape(mu, Srie_P_Fant.GeoS.Rhos, Srie_P_Fant.GeoS.Slices, ...
                                    Srie_P_Fant.GeoS.Thetas)}));
                                f = plot(resL1.cvCart(Fant.nbPixels, Fant.nbPixels));
                                print(f, [filename '_iter' num2str(plotIterMax(i))], '-dpng');
                                close(f);
                            end

                            if strcmpi(Pcnds{PcndIdx}, '')
                                    mu = PDS.x;
                                else
                                    mu = real(Pcnds{PcndIdx}.Direct(PDS.x));
                            end
                            resL1 = Srie_Pol(containers.Map({'GeoS', 'GPhy', ...
                                'Patient', 'Study', 'SeriesDescription', 'Historique', ...
                                'Val'}, ...
                                {Srie_P_Fant.GeoS, GPhy_AttRX(containers.Map('Unites', 'cm-1')), ...
                                Sino.Patient, Sino.Study, ...
                                ['Condat, coord. cyl., iter ' num2str(iters(end))], ...
                                {''}, ...
                                reshape(mu, Srie_P_Fant.GeoS.Rhos, Srie_P_Fant.GeoS.Slices, ...
                                Srie_P_Fant.GeoS.Thetas)}));
                            f = plot(resL1.cvCart(Fant.nbPixels, Fant.nbPixels));
                            print(f, [filename '_iter' num2str(iters(end))], '-dpng');
                            close(f);

                            f = figure('Name', 'Condat, coord. cyl.');
                            semilogy(iters(feass>0), 1e7*ones(size(iters(feass>0))), 'bo', 'MarkerFaceColor','b');
                            hold on
                            semilogy(iters(1:end-1), real(objs(1:end-1)), 'r');
                            grid on
                            axis([iters(1) iters(end-1) 1e3 1e7])
                            xlabel('Iter')
                            ylabel('Obj')
                            hold off
                            print(f, [filename '_Obj'], '-dpng');
                            close(f);
                            
                            f = figure('Name', 'Condat, coord. cyl.');
                            semilogy(iters(feass>0), ones(size(iters(feass>0))), 'bo', 'MarkerFaceColor','b');
                            hold on
                            semilogy(iters(1:end-1), varDiffs(1:end-1), 'r');
                            grid on
                            axis([iters(1) iters(end-1) 1e-8 1])
                            xlabel('Iter')
                            ylabel('VarDiff')
                            hold off
                            print(f, [filename '_VarDiff'], '-dpng');
                            close(f);

                            fileID = fopen([filename '.txt'], 'w');
                            fprintf(fileID, 'Total time : %10d\n', round(PDS.solveTime));
                            fprintf(fileID, 'Last iter :  %10d\n', iters(end-1));
                            fprintf(fileID, 'Best iter :  %10d\n', iters(end));
                            fprintf(fileID, 'First varDiff :  %15.5e\n', real(varDiffs(2)));
                            fprintf(fileID, 'Final varDiff :  %15.5e\n', real(varDiffs(end)));
                            fprintf(fileID, 'First obj :  %15.5e\n', real(objs(1)));
                            fprintf(fileID, 'Final obj :  %15.5e\n', real(objs(end)));
                            fprintf(fileID, PDS.exitFlag);
                            fclose(fileID);
                            cd(ROOTDIR);
                            
                        end
                    end
                end
            end
        end
    end
end

%% Nettoyage
delete(MP_P)
h = get(0, 'Children');
for handle = h'
    figure(handle)
end