%% Proximal Tests

% lb = [1 2 3 4 5 6];
% ub = [10 10 10 10 Inf 10];
% 
% arg1 = lb;
% arg2 = ub;
% arg3 = -lb - 1;
% arg4 = [.999999999 4 5 6 7 8];
% arg5 = [12 10 10 10 7 9];
% 
% gamma = 2;
% lambda = 1;

% disp(utils.Proximal.RectangleIndicator(lb, ub, arg1) == 0);
% disp(utils.Proximal.RectangleIndicator(lb, ub, arg2) == 0);
% disp(utils.Proximal.RectangleIndicator(lb, ub, arg3) == 0);
% disp(utils.Proximal.RectangleIndicator(lb, ub, arg4) == Inf);
% disp(utils.Proximal.RectangleIndicator(lb, ub, arg5) == Inf);
% 
% disp(utils.Proximal.ProxRectangleIndicator(lb, ub, arg1) - arg1);
% disp(utils.Proximal.ProxRectangleIndicator(lb, ub, arg2) - arg2);
% disp(utils.Proximal.ProxRectangleIndicator(lb, ub, arg3) - arg3);
% disp(utils.Proximal.ProxRectangleIndicator(lb, ub, arg4) - arg4);
% disp(utils.Proximal.ProxRectangleIndicator(lb, ub, arg5) - arg5);
% 
% disp(utils.Proximal.ProxConjRectangleIndicator(gamma, lb, ub, arg1));
% disp(utils.Proximal.ProxConjRectangleIndicator(gamma, lb, ub, arg2));
% disp(utils.Proximal.ProxConjRectangleIndicator(gamma, lb, ub, arg3));
% disp(utils.Proximal.ProxConjRectangleIndicator(gamma, lb, ub, arg4));
% disp(utils.Proximal.ProxConjRectangleIndicator(gamma, lb, ub, arg5));
% 
% disp(utils.Proximal.ProxL1(gamma, arg1));
% disp(utils.Proximal.ProxL1(gamma, arg2));
% disp(utils.Proximal.ProxL1(gamma, arg3));
% disp(utils.Proximal.ProxL1(gamma, arg4));
% disp(utils.Proximal.ProxL1(gamma, arg5));
% 
% disp(utils.Proximal.ProxConjL1(gamma, arg1));
% disp(utils.Proximal.ProxConjL1(gamma, arg2));
% disp(utils.Proximal.ProxConjL1(gamma, arg3));
% disp(utils.Proximal.ProxConjL1(gamma, arg4));
% disp(utils.Proximal.ProxConjL1(gamma, arg5));
% 
% disp(utils.Proximal.ProxL2(gamma, arg1));
% disp(utils.Proximal.ProxL2(gamma, arg2));
% disp(utils.Proximal.ProxL2(gamma, arg3));
% disp(utils.Proximal.ProxL2(gamma, arg4));
% disp(utils.Proximal.ProxL2(gamma, arg5));
% 
% disp(utils.Proximal.ProxConjL2(gamma, lambda, arg1));
% disp(utils.Proximal.ProxConjL2(gamma, lambda, arg2));
% disp(utils.Proximal.ProxConjL2(gamma, lambda, arg3));
% disp(utils.Proximal.ProxConjL2(gamma, lambda, arg4));
% disp(utils.Proximal.ProxConjL2(gamma, lambda, arg5));

%% NonDiffConvexModel Tests

% x0 = [65 1 5 6 225 6];
% 
% A = [7 -9 3 15 2 0; -4 -2 8 6 0 7; 1 1 1 0 -7 -2; -9 14 2 6 -8 -8; 3 2 1 -1 -2 -3; 0 0 0 10 0 9];
% 
% ndcm = model.NonDiffConvexModel('bob', @fgradf_test, ...
%     @(arg) utils.Proximal.RectangleIndicator(3*ones(size(arg)), Inf(size(arg)), arg), ...
%     @(gamma, arg) utils.Proximal.ProxRectangleIndicator(3*ones(size(arg)), Inf(size(arg)), arg), ...
%     {@(arg) 5 * norm(arg, 1), @(arg) 0}, {@(gamma, arg) utils.Proximal.ProxConjL1(5, arg), @(gamma, arg) 0},...
%     {@(arg) A * arg, @(arg) arg}, {@(arg) A' * arg, @(arg) arg},...
%     x0);
% 
% PDS = solvers.CondatPDSSolver('bob solver', ndcm, 1.0e-12, 1.0e-8, 1000, Inf, 1, .05, .05, 'A', 2);
% PDS.solve();

