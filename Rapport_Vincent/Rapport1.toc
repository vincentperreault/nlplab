\contentsline {section}{\numberline {1}Probl\`eme}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Principe et configuration exp\'erimentale}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Formulation math\'ematique}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Introduction du bruit et estimation}{4}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}G\'eom\'etrie cylindrique}{6}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}P\'enalisation}{6}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}M\'ethodes de r\'esolution proximales}{9}{subsection.1.6}
\contentsline {section}{\numberline {2}Algorithmes proximaux}{10}{section.2}
\contentsline {subsection}{\numberline {2.1}Op\'erateur de proximit\'e}{10}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}D\'ecomposition avant-arri\`ere}{12}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}D\'ecomposition primale-duale}{14}{subsection.2.3}
\contentsline {section}{\numberline {3}Application}{22}{section.3}
\contentsline {subsection}{\numberline {3.1}Analyse d'application en microscopie par illumination structur\'ee}{22}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Application \`a notre probl\`eme}{26}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Pr\'econditionnement}{30}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Conditions suffisantes de convergence}{33}{subsection.3.4}
\contentsline {section}{\numberline {4}R\'esultats}{38}{section.4}
\contentsline {subsection}{\numberline {4.1}Probl\`eme original}{38}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Probl\`eme avec p\'enalisation $l_2$}{41}{subsection.4.2}
\contentsline {section}{\numberline {5}Conclusion}{45}{section.5}
