classdef YanPDSSolver < solvers.PrimalDualSplitSolver
    %CONDATPDSSOLVER solves NonDiffConvexModel type problems.
    %
    % p : primal stepsize
    % d : dual stepsize
    %
    % This algorithm converges for NonDiffConvexModel if the following
    % conditions are met:
    %
    % p > 0    d > 0
    %
    %                 2
    % p < 2/||grad F||
    %                 2
    %
    %           m          -1
    % d < (p ||sum L*L || )
    %          i=1  i i  2
    %
    properties (SetAccess = private, Hidden = false)
        %relaxParam      % Relaxation parameter (would need additionnal
        %gradF evaluations)
        primStepsize    % Primal stepsize
        dualStepsize    % Dual stepsize
    end
    
    methods (Access = public)
        
        function self = YanPDSSolver(name, ndcm, rVarTol, rObjTol, maxIter, maxRT, primStepsize, dualStepsize, verbose, iScreenshots)
            
            if nargin < 10
                iScreenshots = [];
                if nargin < 9
                    verbose = 0;
                end
            end
            
            self = self@solvers.PrimalDualSplitSolver(name, ndcm, rVarTol, rObjTol, maxIter, maxRT, verbose, iScreenshots);
            
            self.primStepsize = primStepsize;
            self.dualStepsize = dualStepsize;
        end
        
    end
    
    methods (Access = public, Hidden = true)
        
        function [xNew, yNew, FNew, gradFNew] = Update(self, xCurr, yCurr, gradFCurr)
            sumLsy = 0;
            for i = 1:self.ndcm.m
                sumLsy = sumLsy + self.ndcm.evalAdjointL_i(i, yCurr(self.ndcm.d(i):self.ndcm.d(i+1)-1));
            end
            xNew = xCurr - self.primStepsize * (gradFCurr + sumLsy);
            xNew = self.ndcm.evalProxG(self.primStepsize, xNew);
            [FNew, gradFNew] = self.ndcm.evalFGradF(xNew);
            xTild = 2 * xNew - xCurr + self.primStepsize * (gradFCurr - gradFNew);
            yNew = zeros(size(yCurr));
            for i = 1:self.ndcm.m
                yTild = yCurr(self.ndcm.d(i):self.ndcm.d(i+1)-1) + self.dualStepsize * self.ndcm.evalDirectL_i(i, xTild);
                yNew(self.ndcm.d(i):self.ndcm.d(i+1)-1) = self.ndcm.evalProxConjH_i(i, self.dualStepsize, yTild);
            end
        end
        
    end
    
end

