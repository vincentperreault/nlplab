classdef PrimalDualSplitSolver < handle
    %% PrimalDualSplitSolver
    % General primal-dual split model solver. Solves NonDiffConvexModels.
    
    
    properties (SetAccess = protected, Hidden = false)
        ndcm;       % Original NonDiffConvexModel object
        name = 'generic';       % Solver name
        rVarTol = 1.0e-5;   % Relative stopping tolerance on primal solution update difference (in 1D)
        rObjTol = 1.0e-8;    % Relative stopping tolerance on objective update difference
        maxIter = Inf;    % Maximum number of iterations
        verbose = 0;    % 0, 1 or 2
        maxRT = Inf;      % Maximum run time
        maxObjIncCnt = Inf;%10; % Maximum number of successive objective function increases
        x;          % current primal point upon termination
        y;          % current dual points upon termination
        obj;        % objective function value at x
        exitFlag;   % Exit flag
        solved;     % bool flag that indicates if problem has been solved
        solveTime;  % cpu time to complete the solve method
        status;     % status after solve
        iter;       % current iteration count
        iScreenshots = [];
        logger = struct('Iter', [], 'varDiff', [], 'Obj', [], 'Feas', [], 'Screenshots', []); % logger object
    end
    
    properties (Constant, Hidden = true)
        % Exit flags
        % Solved := {1, 2}
        EXIT_NONE = 0;
        EXIT_VAR_TOL = 1;
        EXIT_OBJ_TOL = 2;
        EXIT_MAX_ITER = 3;
        EXIT_MAX_RT = 4;
        EXIT_INNER_FAIL = 5;
        EXIT_UNKNOWN = 6;
        EXIT_MAX_OBJ_INC = 7;
        % Exit messages
        EXIT_MSG = { ...
            'Primal and dual solutions changing by less than primTol\n', ...% 1
            'Objective function value changing by less than ObjTol\n', ...  % 2
            'Maximum number of iterations reached\n', ...                   % 3
            'Maximum run time reached\n', ...                               % 4
            'Inner iteration failed to converge\n', ...                     % 5
            'Unknown exit\n', ...                                           % 6
            'Objective function successively increased the maximum amount of times\n', ...% 7
            };
    end
    
    
    methods (Access = public)
        
        function self = PrimalDualSplitSolver(name, ndcm, rVarTol, rObjTol, maxIter, maxRT, verbose, iScreenshots)
            %% Constructor
            
            % Verifying that nlp is a subclass of nlpmodel
            if ~isa(ndcm, 'model.NonDiffConvexModel')
                error('ndcm should be a NonDiffConvexModel');
            end
            
            self.name = name;
            self.ndcm = ndcm;
            self.rVarTol = rVarTol;
            self.rObjTol = rObjTol;
            self.maxIter = maxIter;
            self.maxRT = maxRT;
            self.verbose = verbose;
            if nargin == 8
                self.iScreenshots = iScreenshots;
            end
            
            self.exitFlag = 'Solving has not been attempted.';
            self.iter = 0;
            self.status = 'unknown';
            self.solved = false;
            self.solveTime = Inf;
        end
        
        function solve(self)
            self.ndcm.resetCounters();
            time = tic;
            self.status = 'solving';
            
            self.x = self.ndcm.x0;
            self.y = self.ndcm.y0;
            
            [F, gradF] = self.ndcm.evalFGradF(self.x);
            [self.obj, feasible] = self.evalObj(F, self.x);
            objTol = self.rObjTol * self.obj;
            varTol = 0;
            
            xOld = Inf(size(self.x));
            yOld = self.y;
            objOld = Inf;
            
            if self.verbose >= 2
                line = sprintf('%10s %15s %15s %15s %15s %10s\n', 'Iter', 'VarDiff', 'Obj', 'Infeas');
                fprintf(line);
            end
            
            bestX = self.x;
            bestIt = 0;
            
            while true
                varDiff = sqrt((norm(self.x - xOld, 2).^2 + norm(self.y - yOld, 2).^2)/(self.ndcm.p + self.ndcm.d(end)-1));%norm(self.x - xOld, 2) / sqrt(self.ndcm.p);
                self.logger.Iter(end+1) = self.iter;
                self.logger.varDiff(end+1) = varDiff;
                self.logger.Obj(end+1) = self.obj;
                self.logger.Feas(end+1) = feasible;
                if self.verbose >= 2
                    if feasible
                        line = sprintf('%10d %15.5e %15.5e\n', self.iter, varDiff, self.obj);
                    else
                        line = sprintf('%10d %15.5e %15.5e %10s\n', self.iter, varDiff, self.obj, 'X');
                    end
                    fprintf(line);
                end
                if self.obj > objOld
                    objIncCnt = objIncCnt + 1;
                else
                    objIncCnt = 0;
                    if self.obj < self.logger.Obj(bestIt + 1)
                        bestX = self.x;
                        bestIt = self.iter;
                    end
                end
                if ~isempty(self.iScreenshots) && self.iter >= self.iScreenshots(1)
                    self.logger.Screenshots = [self.logger.Screenshots, self.x];
                    self.iScreenshots = self.iScreenshots(2:end);
                end
                if self.iter == 1
                   varTol = self.rVarTol * varDiff;
                end
                
                if self.iter >= self.maxIter
                    self.exitFlag = self.EXIT_MSG{self.EXIT_MAX_ITER};
                    break;
                end
                if toc(time) >= self.maxRT
                    self.exitFlag = self.EXIT_MSG{self.EXIT_MAX_RT};
                    break;
                end
                if objIncCnt == self.maxObjIncCnt
                    self.exitFlag = self.EXIT_MSG{self.EXIT_MAX_OBJ_INC};
                    break;
                end
                if varDiff < varTol
                    self.exitFlag = self.EXIT_MSG{self.EXIT_VAR_TOL};
                    self.solved = true;
                    break;
                end
                if abs(self.obj - objOld) < objTol
                    self.exitFlag = self.EXIT_MSG{self.EXIT_OBJ_TOL};
                    self.solved = true;
                    break;
                end
                
                objOld = self.obj;
                xOld = self.x;
                yOld = self.y;
                
                [self.x, self.y, F, gradF] = self.Update(self.x, self.y, gradF);
                
                [self.obj, feasible] = self.evalObj(F, self.x);
                
                self.iter = self.iter + 1;
            end
            
            if bestIt < self.iter
                self.x = bestX;
                self.obj = self.logger.Obj(bestIt + 1);
                self.logger.Iter(end+1) = self.logger.Iter(bestIt+1);
                self.logger.varDiff(end+1) = self.logger.varDiff(bestIt+1);
                self.logger.Obj(end+1) = self.logger.Obj(bestIt+1);
                self.logger.Feas(end+1) = self.logger.Feas(bestIt+1);
            else
                self.logger.Iter(end+1) = self.logger.Iter(end);
                self.logger.varDiff(end+1) = self.logger.varDiff(end);
                self.logger.Obj(end+1) = self.logger.Obj(end);
                self.logger.Feas(end+1) = self.logger.Feas(end);
            end
            
            if self.verbose >= 2
                fprintf(self.exitFlag);
                if self.logger.Feas(end)
                    line = sprintf('Best:%5d %15.5e %15.5e\n', self.logger.Iter(end), self.logger.varDiff(end), self.logger.Obj(end));
                else
                    line = sprintf('Best:%5d %15.5e %15.5e %10s\n', self.logger.Iter(end), self.logger.varDiff(end), self.logger.Obj(end), 'X');
                end
                fprintf(line);
            end
            
            if self.solved
                self.status = 'done';
            else
                self.status = [self.name, ' failed to converge.'];
            end
            self.solveTime = toc(time);
        end
        
        function [value, feasible] = evalObjFeas(self, x)
            [F, ~] = self.ndcm.evalFGradF(x);
            [value, feasible] = self.evalObj(F, x);
        end
        
    end
    
    methods (Access = private)
        
        function [value, feasible] = evalObj(self, F, x)
            value = F;
            feasible = true;
            additionalTerm = self.ndcm.evalG(x);
            if additionalTerm < Inf
                value = value + additionalTerm;
            else
                feasible = false;
            end
            for i = 1:self.ndcm.m
                additionalTerm = self.ndcm.evalH_i(i, self.ndcm.evalDirectL_i(i, x));
                if additionalTerm < Inf
                    value = value + additionalTerm;
                else
                    feasible = false;
                end
            end
        end
        
    end
    
    % List of abstract methods that must be defined in a subclass
    methods (Abstract)
        
        [xNew, yNew, FNew, gradFNew] = Update(self, xCurr, yCurr, gradFCurr)
        
    end
    
end
