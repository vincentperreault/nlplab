classdef CondatPDSSolver < solvers.PrimalDualSplitSolver
    %CONDATPDSSOLVER solves NonDiffConvexModel type problems.
    %
    % r : relaxation parameter
    % p : primal stepsize
    % d : dual stepsize
    %
    % Both versions of this algorithm converge for NonDiffConvexModel if
    % the following conditions are met:
    %
    % r > 0    p > 0    d > 0
    %
    %            m                   2
    % 1/p - d ||sum L*L ||  >= ||grad F|| /2
    %           i=1  i i  2              2
    %
    %               2                   m          -1
    % r < 2 - ||grad F|| /2 (1/p - d ||sum L*L || )   in [1, 2[
    %                   2              i=1  i i  2
    %
    properties (SetAccess = private, Hidden = false)
        version % Version of the algorithm, either 'A' or 'B'
        
        relaxParam      % Relaxation parameter
        primStepsize    % Primal stepsize
        dualStepsize    % Dual stepsize
    end
    
    methods (Access = public)
        
        function self = CondatPDSSolver(name, ndcm, rVarTol, rObjTol, maxIter, maxRT, relaxParam, primStepsize, dualStepsize, version, verbose, iScreenshots)
            
            if nargin < 12
                iScreenshots = [];
                if nargin < 11
                    verbose = 0;
                    if nargin < 10
                        version = 'A';
                    end
                end
            end
            
            self = self@solvers.PrimalDualSplitSolver(name, ndcm, rVarTol, rObjTol, maxIter, maxRT, verbose, iScreenshots);
            
            self.version = version;
            self.relaxParam = relaxParam;
            self.primStepsize = primStepsize;
            self.dualStepsize = dualStepsize;
        end
        
    end
    
    methods (Access = public, Hidden = true)
        
        function [xNew, yNew, FNew, gradFNew] = Update(self, xCurr, yCurr, gradFCurr)
            if self.version == 'A'
                [xNew, yNew] = self.UpdateA(xCurr, yCurr, gradFCurr);
            else
                [xNew, yNew] = self.UpdateB(xCurr, yCurr, gradFCurr);
            end
            [FNew, gradFNew] = self.ndcm.evalFGradF(xNew);
        end
        
    end
    
    methods (Access = private)
        
        function [xNew, yNew] = UpdateA(self, xCurr, yCurr, gradFCurr)
            sumLsy = 0;
            for i = 1:self.ndcm.m
                sumLsy = sumLsy + self.ndcm.evalAdjointL_i(i, yCurr(self.ndcm.d(i):self.ndcm.d(i+1)-1));
            end
            xTild = xCurr - self.primStepsize * (gradFCurr + sumLsy);
            xTild = self.ndcm.evalProxG(self.primStepsize, xTild);
            xNew = self.relaxParam * xTild + (1 - self.relaxParam) * xCurr;
            yNew = zeros(size(yCurr));
            for i = 1:self.ndcm.m
                yTild = yCurr(self.ndcm.d(i):self.ndcm.d(i+1)-1) + self.dualStepsize * self.ndcm.evalDirectL_i(i, 2 * xTild - xCurr);
                yTild = self.ndcm.evalProxConjH_i(i, self.dualStepsize, yTild);
                yNew(self.ndcm.d(i):self.ndcm.d(i+1)-1) = self.relaxParam * yTild + (1 - self.relaxParam) * yCurr(self.ndcm.d(i):self.ndcm.d(i+1)-1);
            end
        end
        
        function [xNew, yNew] = UpdateB(self, xCurr, yCurr, gradFCurr)
            yNew = zeros(size(yCurr));
            sumLsy = 0;
            for i = 1:self.ndcm.m
                yTild = yCurr(self.ndcm.d(i):self.ndcm.d(i+1)-1) + self.dualStepsize * self.ndcm.evalDirectL_i(i, xCurr);
                yTild = self.ndcm.evalProxConjH_i(i, self.dualStepsize, yTild);
                yNew(self.ndcm.d(i):self.ndcm.d(i+1)-1) = self.relaxParam * yTild + (1 - self.relaxParam) * yCurr(self.ndcm.d(i):self.ndcm.d(i+1)-1);
                sumLsy = sumLsy + self.ndcm.evalAdjointL_i(i, 2 * yTild - yCurr(self.ndcm.d(i):self.ndcm.d(i+1)-1));
            end
            xTild = xCurr - self.primStepsize * (gradFCurr + sumLsy);
            xTild = self.ndcm.evalProxG(self.primStepsize, xTild);
            xNew = self.relaxParam * xTild + (1 - self.relaxParam) * xCurr;
        end
        
    end
    
end

