%% Initialisation

close all;
clear all;
clc;

global ROOTDIR
ROOTDIR = fullfile(getenv('HOME'), 'projet');
DATADIR = 'projData';
MPDIR = fullfile(DATADIR, 'projMatrices');
addpath(genpath(fullfile(ROOTDIR, 'Objets')))
addpath(genpath(fullfile(ROOTDIR, 'scriptsreconstruction')))
addpath(genpath(fullfile(ROOTDIR, 'sparse')))
addpath(genpath(fullfile(ROOTDIR, 'NLPLab')))

%% G�n�ration ou lecture des donn�es XCAT
[Fant, Sino] = GenerationDonnees2D(DATADIR);

GeoObj = 'Circ';
Srie_Fant = Fant.creationSerie(DATADIR, 'GeoObj', GeoObj);

%% Cr�ation de la matrice de projection
nRayons = 3;
SparseRep = 'ICCS';
SparseMode = 'MatVec';
[~, maxThreads] = nCPUs();

Thetas = Sino.GeoP.nProjections;
Rhos = round(Srie_Fant.GeoS.Rows * Srie_Fant.GeoS.Columns / ...
    Thetas);
Srie_P_Fant = Srie_Fant.cvPol(Rhos, Thetas);

GeoM_P = GeoM_Factory.create(Srie_P_Fant.GeoS, Sino, ...
    'nRayons', nRayons);

[s, message, ~] = mkdir(MPDIR);

MP_P = MP_Pol(containers.Map({'GeoM', 'nThreads', 'SparseRep', ...
    'SparseMode', 'MatDir'}, ...
    {GeoM_P, maxThreads, SparseRep, SparseMode, MPDIR}));

%% Reconstruction par FBP, en coordonn�es polaires

FBP_P = FBP(containers.Map({'MatP', 'GeoS', 'Sino'}, ...
    {MP_P, Srie_P_Fant.GeoS, Sino}));
recFBP_P = FBP_P.reconst();

plot(recFBP_P.cvCart(Fant.nbPixels, Fant.nbPixels));

%% Param�tres de l'algorithme L-BFGS-B
max_iter = 1000;
epsilon = 1e-05;

%% Param�tres du crit�re en coordonn�es polaires
lambdaL2_P = 1e-02;
facL2L1_P = 1; 
delta_P = 5e-03;
% Pour pouvoir comparer les facteurs de r�gularisation L2 et L2L1
lambdaL2L1_P = facL2L1_P * 2 * delta_P * lambdaL2_P;

%% G�n�ration d'un crit�re en coordonn�es polaires
Adeq = Linear_Gauss_noPond(containers.Map('MPrj', MP_P));
PenalGradObj = PenalGradObj_L2(containers.Map(...
    {'GeoS', 'lambda', 'delta'},...
    {Srie_P_Fant.GeoS, lambdaL2_P, delta_P}));
crit_P = Critere(containers.Map('J', {Adeq, PenalGradObj}));

%% Reconstruction par L-BFGS-B en coordonn�s polaires
Pcnd_Id = Precond_Factory.create('Identite');
% [recLBFGSB_P1, recInfo_P1] = reconstLBFGSB_P(Sino, crit_P, Pcnd_Id, ...
%     max_iter, epsilon, [], {recFBP_P.Val(:)});
% plot(recLBFGSB_P1.cvCart(Fant.nbPixels, Fant.nbPixels))

Pcnd_L2 = Precond_Factory.create('DiagF', 'crit', crit_P);
[recLBFGSB_P2, recInfo_P2] = reconstLBFGSB_P(Sino, crit_P, Pcnd_L2, ...
    max_iter, epsilon, [], {zeros(226, 1, 1160)});
f = plot(recLBFGSB_P2.cvCart(Fant.nbPixels, Fant.nbPixels));
print(f, 'Fantome354354', '-dpng');
close(f);

%% Calcul de bornes des normes matricielles

%Norm of preconditioner
normCId = 1;
normCL2 = max(abs(Pcnd_L2.Mdiag(:)));

%Norm of adequation ponderation
normSigma = 1;

%Bound on norm of first difference matrix
sqNormD = 4*((Thetas/(2*pi)).^2 + Rhos);

%Norm of projection matrix
sqNormP = 2282.685;

%% Paramètres PDS

rPrimTol = 0;
rObjTol = 8932.3245/64564.3056;
relaxParam = 1;
lambdaPSS = 0.99;

primMaxStpSzId = 2 * min(2 - relaxParam, 1) / (2 * normCId.^2 * sqNormP * normSigma);
primStpSzId = lambdaPSS * primMaxStpSzId;
dualStpSzId = 0.9999*(1/primStpSzId - 1/primMaxStpSzId) / (normCId.^2 * (1 + sqNormD));

primMaxStpSzL2 = 2 * min(2 - relaxParam, 1) / (2 * normCL2.^2 * sqNormP * normSigma);
primStpSzL2 = lambdaPSS * primMaxStpSzL2;
dualStpSzL2 = 0.9999*(1/primStpSzL2 - 1/primMaxStpSzL2) / (normCL2.^2 * (1 + sqNormD));

%% Condat

ndrm_Id = model.NonDiffRecModel('NDRM Identite', crit_P, Pcnd_Id, Sino, Srie_P_Fant.GeoS, recFBP_P.Val(:), 0);
PDS_Id = solvers.CondatPDSSolver('Condat Sans Prec', ndrm_Id, rPrimTol, rObjTol, Inf, Inf, relaxParam, primStpSzId, dualStpSzId, 'B', 2);
PDS_Id.solve();
resId = Srie_Pol(containers.Map({'GeoS', 'GPhy', ...
    'Patient', 'Study', 'SeriesDescription', 'Historique', ...
    'Val'}, ...
    {Srie_P_Fant.GeoS, GPhy_AttRX(containers.Map('Unites', 'cm-1')), ...
    Sino.Patient, Sino.Study, ...
    'Condat Id', ...
    {''}, ...
    reshape(real(PDS_Id.x), Srie_P_Fant.GeoS.Rhos, Srie_P_Fant.GeoS.Slices, ...
    Srie_P_Fant.GeoS.Thetas)}));
plot(resId.cvCart(Fant.nbPixels, Fant.nbPixels));

ndrm_L2 = model.NonDiffRecModel('NDRM Identite', crit_P, Pcnd_L2, Sino, Srie_P_Fant.GeoS, recFBP_P.Val(:), 0);
PDS_L2 = solvers.CondatPDSSolver('Condat Sans Prec', ndrm_L2, rPrimTol, rObjTol, Inf, Inf, relaxParam, primStpSzL2, dualStpSzL2, 'B', 2);
PDS_L2.solve();
resL2 = Srie_Pol(containers.Map({'GeoS', 'GPhy', ...
    'Patient', 'Study', 'SeriesDescription', 'Historique', ...
    'Val'}, ...
    {Srie_P_Fant.GeoS, GPhy_AttRX(containers.Map('Unites', 'cm-1')), ...
    Sino.Patient, Sino.Study, ...
    'Condat DiagF', ...
    {''}, ...
    reshape(real(Pcnd_L2.Direct(PDS_L2.x)), Srie_P_Fant.GeoS.Rhos, Srie_P_Fant.GeoS.Slices, ...
    Srie_P_Fant.GeoS.Thetas)}));
plot(resL2.cvCart(Fant.nbPixels, Fant.nbPixels));

%% R�sultats de convergence
% figure('Name', 'Convergence')
% semilogy(recInfo_P1.iter_hist(:,1), recInfo_P1.iter_hist(:,2),...
%     'r', 'LineWidth', 2), hold on
% semilogy(recInfo_P2.iter_hist(:,1), recInfo_P2.iter_hist(:,2),...
%     'b', 'LineWidth', 2), hold on
% semilogy(PDS_Id.logger.Iter(1:end-1), PDS_Id.logger.Obj(1:end-1),...
%     'g', 'LineWidth', 2), hold on
% semilogy(PDS_L2.logger.Iter(1:end-1), PDS_L2.logger.Obj(1:end-1),...
%     'y', 'LineWidth', 2), hold on
% hold off
% legend('L-BFGS-B', 'Prec. L-BFGS-B', 'Condat', 'Prec. Condat', 'location', 'NE')
% title('Distance relative � la solution L2', 'FontSize', 16)
% xlabel('It�rations', 'FontSize', 14)
% ylabel('Distance quadratique relative', 'FontSize', 14)
% grid on
                
%% Nettoyage
delete(crit_P.J{1}.MPrj)
h = get(0, 'Children');
for handle = h'
    figure(handle)
end