function [f, gradf] = fgradf_test(arg)
    f = norm(arg,2);
    gradf = 2*arg;
end

