%% Initialisation

close all;
clear all;
clc;

global ROOTDIR
ROOTDIR = fullfile(getenv('HOME'), 'projet');
DATADIR = 'projData';
MPDIR = fullfile(DATADIR, 'projMatrices');
addpath(genpath(fullfile(ROOTDIR, 'Objets')))
addpath(genpath(fullfile(ROOTDIR, 'scriptsreconstruction')))
addpath(genpath(fullfile(ROOTDIR, 'sparse')))
addpath(genpath(fullfile(ROOTDIR, 'NLPLab')))

%% G�n�ration ou lecture des donn�es XCAT
[Fant, Sino] = GenerationDonnees2D(DATADIR);

GeoObj = 'Circ';
Srie_Fant = Fant.creationSerie(DATADIR, 'GeoObj', GeoObj);

%% Cr�ation de la matrice de projection
nRayons = 3;
SparseRep = 'ICCS';
SparseMode = 'MatVec';
[~, maxThreads] = nCPUs();

Thetas = Sino.GeoP.nProjections;
Rhos = round(Srie_Fant.GeoS.Rows * Srie_Fant.GeoS.Columns / ...
    Thetas);
Srie_P_Fant = Srie_Fant.cvPol(Rhos, Thetas);

GeoM_P = GeoM_Factory.create(Srie_P_Fant.GeoS, Sino, ...
    'nRayons', nRayons);

[s, message, ~] = mkdir(MPDIR);

MP_P = MP_Pol(containers.Map({'GeoM', 'nThreads', 'SparseRep', ...
    'SparseMode', 'MatDir'}, ...
    {GeoM_P, maxThreads, SparseRep, SparseMode, MPDIR}));

%% Reconstruction par FBP, en coordonn�es polaires

FBP_P = FBP(containers.Map({'MatP', 'GeoS', 'Sino'}, ...
    {MP_P, Srie_P_Fant.GeoS, Sino}));
recFBP_P = FBP_P.reconst();

% plot(recFBP_P.cvCart(Fant.nbPixels, Fant.nbPixels));

%% Construction des crit�res polaires L2 & L2L1
lambdaL2_P = 1e-02;
AdeqL2 = Linear_Gauss_noPond(containers.Map('MPrj', MP_P));
PenalGradObjL2 = PenalGradObj_L2(containers.Map(...
    {'GeoS', 'lambda'},...
    {Srie_P_Fant.GeoS, lambdaL2_P}));
crit_L2 = Critere(containers.Map('J', {AdeqL2, PenalGradObjL2}));
Pcnd_L2 = Precond_Factory.create('DiagF', 'crit', crit_L2);

% lambdaL2L1_P = 1e-04;
% deltaL2L1_P = 5e-03;
% PenalGradObjL2L1 = PenalGradObj_L2L1(containers.Map(...
%     {'GeoS', 'lambda', 'delta'},...
%     {Srie_P_Fant.GeoS, lambdaL2L1_P, deltaL2L1_P}));
% crit_L2L1 = Critere(containers.Map('J', {AdeqL2, PenalGradObjL2L1}));
% Pcnd_L2L1 = Precond_Factory.create('DiagF', 'crit', crit_L2L1);

Pcnd_Id = Precond_Factory.create('Identite');

%% Boucle Test

init = recFBP_P.Val(:);
relaxParam = 1;
lambdaPSS = 0.9;
plotIterMax = 10000;
version = 'B';

cd('~/Desktop/TestData');
plot(Srie_Fant);
                
%% Calcul de bornes des normes matricielles

%Norm of preconditioner
normC = max(abs(Pcnd_L2.Mdiag(:)));

%Norm of adequation ponderation
normSigma = max(max(exp(-Sino.Scans{1}(:))));

%Bound on norm of first difference matrix
sqNormD = 4*((Thetas/(2*pi)).^2 + Rhos);

%Norm of projection matrix
normP = 47.7775;
% Factor = cell(1,1);
% nBlocks = AdeqL1.MPrj.GeoM.nBlocks;
% objBlkSize = AdeqL1.MPrj.GeoM.objBlkSize;
% sinoBlkSize = AdeqL1.MPrj.GeoM.sinoBlkSize;
% 
% MatPFile = fullfile(MP_P.MatDir, MP_P.Fichier);
% load(MatPFile, 'IlIcV')
% Factor{1} = BCirc(containers.Map({'Blocks', 'BlkRows', 'BlkCols', ...
%     'Val'}, ...
%     {nBlocks, sinoBlkSize, objBlkSize, ...
%     sparse(IlIcV(:, 1), IlIcV(:, 2), IlIcV(:, 3), sinoBlkSize, ...
%     objBlkSize * nBlocks)}));
% clear IlIcV
% 
% nThreads = AdeqL1.MPrj.nThreads;
% Granul = ceil(physicalMemory() / 8 / 4 / objBlkSize^2);
% for idx = 1:Granul:nBlocks
%     iDBlks = (idx:min(idx+Granul-1, nBlocks));
%     DBlocks = DiagBlksFTF(Factor, iDBlks, nThreads);
%     for iBl = 1:length(iDBlks)
%         eigenValues = eig(DBlocks{iBl});
%         sqrtMaxEigen = sqrt(max(abs(eigenValues)));
%         if sqrtMaxEigen > normP
%             normP = sqrtMaxEigen;
%         end
%     end
% end
% clear Factor

%% Paramètres PDS
%                     relax     lmbdaPSS     primSS      dualSS
% Identite :            1          0.9       7.3e-4      4.0e-4
% Diag F :              1          0.9       1.8e-14     4.0e-4
% Diag F + precL2 :     1          0.9       7.4e-6      4.0e-4
% Diag F + precL2L1 :   1          0.9       7.4e-6      4.0e-4
primMaxStpSz = 2 * min(2 - relaxParam, 1) / (2 * normC.^2 * normP.^2);
primStpSz = lambdaPSS * primMaxStpSz;
dualStpSz = (1/primStpSz - 1/primMaxStpSz) / (normC.^2 * (1 + sqNormD));

aPrimTol = 1e-8;
rObjTol = 1e-8;

%% Reconstruction PDS
Pcnds = {Pcnd_Id, Pcnd_L2};

data = struct('lastIter', [], 'lastObj', [], 'lastXDiff', [], 'time', []);

for i = 1:2

    mu = init;

    resL1 = Srie_Pol(containers.Map({'GeoS', 'GPhy', 'Patient', 'Study', ...
                'SeriesDescription', 'Historique', 'Val'}, ...
                {Srie_P_Fant.GeoS, GPhy_AttRX(containers.Map('Unites', 'cm-1')), ...
                Sino.Patient, Sino.Study, 'Initial', {'Initial'}, ...
                reshape(mu, Srie_P_Fant.GeoS.Rhos, Srie_P_Fant.GeoS.Slices, ...
                Srie_P_Fant.GeoS.Thetas)}));
    plot(resL1.cvCart(Fant.nbPixels, Fant.nbPixels));

    time = tic;

    ndrm = model.NonDiffRecModel('NDRM', crit_L2, Pcnds{i}, Sino, Srie_P_Fant.GeoS, mu, 0);

    %PDS = solvers.CondatPDSSolver('Condat', ndrm, aPrimTol, rObjTol, plotIterMax, Inf, relaxParam, primStpSz, dualStpSz, version, 2);
    PDS = solvers.YanPDSSolver('Condat', ndrm, aPrimTol, rObjTol, plotIterMax, Inf, primStpSz, dualStpSz, 2);

    PDS.solve();

    iters = PDS.logger.Iter;
    xDiffs = PDS.logger.xDiff;
    yDiffs = PDS.logger.yDiff;
    objs = PDS.logger.Obj;
    feass = PDS.logger.Feas;

    mu = real(Pcnds{i}.Direct(PDS.x));

    resL1 = Srie_Pol(containers.Map({'GeoS', 'GPhy', ...
            'Patient', 'Study', 'SeriesDescription', 'Historique', ...
            'Val'}, ...
            {Srie_P_Fant.GeoS, GPhy_AttRX(containers.Map('Unites', 'cm-1')), ...
            Sino.Patient, Sino.Study, ...
            ['Condat, coord. cyl., iter ' num2str(plotIterMax)], ...
            {''}, ...
            reshape(mu, Srie_P_Fant.GeoS.Rhos, Srie_P_Fant.GeoS.Slices, ...
            Srie_P_Fant.GeoS.Thetas)}));
    plot(resL1.cvCart(Fant.nbPixels, Fant.nbPixels));

    figure('Name', 'Condat, coord. cyl.');
    semilogy(iters(feass>0), 1e7*ones(size(iters(feass>0))), 'bo', 'MarkerFaceColor','b');
    hold on
    semilogy(iters(1:end-1), xDiffs(1:end-1), 'r');
    semilogy(iters(1:end-1), yDiffs(1:end-1), 'g');
    semilogy(iters(1:end-1), objs(1:end-1), 'b');
    grid on
    axis([iters(1) iters(end-1) 1e-8 1e7])
    xlabel('Iter')
    ylabel('xDiff(r), yDiff(g) & Obj(b)')
    hold off

    time = toc(time);
    
    data.lastIter(i) = iters(end);
    data.lastObj(i) = objs(end);
    data.lastXDiff(i) = xDiffs(end);
    data.time(i) = time;

end

data

%% Nettoyage
cd(ROOTDIR);
delete(crit_L2.J{1}.MPrj)
h = get(0, 'Children');
for handle = h'
    figure(handle)
end