%% Initialisation

close all;
clear all;
clc;

global ROOTDIR
ROOTDIR = fullfile(getenv('HOME'), 'projet');
DATADIR = 'projData';
MPDIR = fullfile(DATADIR, 'projMatrices');
addpath(genpath(fullfile(ROOTDIR, 'Objets')))
addpath(genpath(fullfile(ROOTDIR, 'scriptsreconstruction')))
addpath(genpath(fullfile(ROOTDIR, 'sparse')))
addpath(genpath(fullfile(ROOTDIR, 'NLPLab')))

%% G�n�ration ou lecture des donn�es XCAT
[Fant, Sino] = GenerationDonnees2D(DATADIR);

GeoObj = 'Circ';
Srie_Fant = Fant.creationSerie(DATADIR, 'GeoObj', GeoObj);

%% Cr�ation de la matrice de projection
nRayons = 3;
SparseRep = 'ICCS';
SparseMode = 'MatVec';
[~, maxThreads] = nCPUs();

Thetas = Sino.GeoP.nProjections;
Rhos = round(Srie_Fant.GeoS.Rows * Srie_Fant.GeoS.Columns / ...
    Thetas);
Srie_P_Fant = Srie_Fant.cvPol(Rhos, Thetas);

GeoM_P = GeoM_Factory.create(Srie_P_Fant.GeoS, Sino, ...
    'nRayons', nRayons);

[s, message, ~] = mkdir(MPDIR);

MP_P = MP_Pol(containers.Map({'GeoM', 'nThreads', 'SparseRep', ...
    'SparseMode', 'MatDir'}, ...
    {GeoM_P, maxThreads, SparseRep, SparseMode, MPDIR}));

%% Calcul de la norme

Factor = cell(1,3);
nBlocks = MP_P.GeoM.nBlocks;
objBlkSize = MP_P.GeoM.objBlkSize;
sinoBlkSize = MP_P.GeoM.sinoBlkSize;
nElemThetas = MP_P.GeoM.nElemThetas;
nThreads = MP_P.nThreads;
Granul = ceil(physicalMemory() / 8 / 4 / objBlkSize^2);

MatPFile = fullfile(MP_P.MatDir, MP_P.Fichier);
load(MatPFile, 'IlIcV')
Factor{1} = BCirc(containers.Map({'Blocks', 'BlkRows', 'BlkCols', ...
    'Val'}, ...
    {nBlocks, sinoBlkSize, objBlkSize, ...
    sparse(IlIcV(:, 1), IlIcV(:, 2), IlIcV(:, 3), sinoBlkSize, ...
    objBlkSize * nBlocks)}));
clear IlIcV

Res = [];
for lambdaL2_P = [5e-3 5e-2 5e-1 5]%[1e-6 5e-6 1e-5 5e-5 1e-4 5e-4 1e-3 5e-3 1e-2 5e-2 1e-1 5e-1 1]
    Mat = Srie_P_Fant.GeoS.MatD1m;
    S.type = '()';
    S.subs = {1:Mat.BlkRows};
    pondRad = sqrt(lambdaL2_P * subsref(Srie_P_Fant.GeoS.J_D1m, S));
    Factor{2} = BCirc(containers.Map({'Blocks', 'BlkRows', 'BlkCols', 'Val'}, ...
        {Mat.Blocks, Mat.BlkRows, Mat.BlkCols, ...
        pondRad(:, ones(1, Mat.BlkCols * Mat.Blocks)) .* Mat.Val}));
    Factor{2} = groupBlocks(Factor{2}, nElemThetas);
    Mat = Srie_P_Fant.GeoS.MatD1n;
    S.subs = {1:Mat.BlkRows};
    pondRad = sqrt(lambdaL2_P * subsref(Srie_P_Fant.GeoS.J_D1n, S));
    Factor{3} = BCirc(containers.Map({'Blocks', 'BlkRows', 'BlkCols', 'Val'}, ...
        {Mat.Blocks, Mat.BlkRows, Mat.BlkCols, ...
        pondRad(:, ones(1, Mat.BlkCols * Mat.Blocks)) .* Mat.Val}));
    Factor{3} = groupBlocks(Factor{3}, nElemThetas);

    NormH_BC = 0;
    NormDiff = 0;
    NormDiffInv = 0;
    NormH_BCInv = 0;
    for idx = 1:Granul:nBlocks
        iDBlks = (idx:min(idx+Granul-1, nBlocks));
        DBlocks = DiagBlksFTF(Factor, iDBlks, nThreads);
        for iBl = 1:length(iDBlks)
            eigenValuesBC = eig(DBlocks{iBl});
            maxEigenBC = max(abs(eigenValuesBC));
            if maxEigenBC > NormH_BC
                NormH_BC = maxEigenBC;
            end
            eigenValuesDiff = eig(DBlocks{iBl} - diag(diag(DBlocks{iBl})));
            maxEigenDiff = max(abs(eigenValuesDiff));
            if maxEigenDiff > NormDiff
                NormDiff = maxEigenDiff;
            end
            eigenValuesBC = eig(inv(DBlocks{iBl}));
            maxEigenBC = max(abs(eigenValuesBC));
            if maxEigenBC > NormH_BCInv
                NormH_BCInv = maxEigenBC;
            end
            eigenValuesDiff = eig(inv(DBlocks{iBl}) - diag(diag(DBlocks{iBl}).^(-1)));
            maxEigenDiff = max(abs(eigenValuesDiff));
            if maxEigenDiff > NormDiffInv
                NormDiffInv = maxEigenDiff;
            end
        end
    end
    clear Factor{2} Factor{3} DBolcks
    
    sprintf('Lambda : %5.0e   =>   Err = %6.4f   ErrInv = %6.4f', lambdaL2_P, NormDiff/NormH_BC, NormDiffInv/NormH_BCInv)
    
    Res(1:2, end + 1) = [NormDiff/NormH_BC; NormDiffInv/NormH_BCInv];

end
clear Factor