classdef NonDiffConvexModel < handle
    %% NonDiffConvexModel - Convex minimization problem with proximable functions
    % Solves the unconstrained problem
    %
    %                      m
    % min [ F(x) + G(x) + sum( H (L  x) ) ]
    %  x                  i=1   i  i
    %
    % where F is convex and differentiablle, G and all H_i are convex and
    % proximable (i.e. their proximity operator is known and easily
    % evaluated) and the L_i are real matrices of appropriate dimensions.
    %
    % The dual problem is
    %
    %                 m              m
    % min [ (F*G*)(-sum( L* y )) + sum( H*(y ) ) ]
    % (y )           i=1   i  i     i=1   i  i
    %   i i in [[1, m]]
    %
    % where * denotes the Fenchel conjugate for F, G and the H_i and the
    % hermitian adjoint for the L_i and  denotes the infimal convolution
    % defined as
    %
    % (F*G*)(x) = inf [ F*(x - x') + G*(x') ]
    %               x'
    %
    % This class can only be passed to the appropriate primal-dual split
    % solvers such as CondatPDSSolver and YanPDSSolver.
    %
    % NonDiffConvexModel(name, F, gradF, G, proxG, H, proxConjH, directL, adjointL, x0, *y0)
    % where * denotes optional arguments
    
    %% Properties
    properties (SetAccess = private, Hidden = false)
        name    % problem name
        m       % number of dual variables (number of H_i)
        p       % dimension of the primal variable x
        d       % matrix of the beginning indices of the dual variables y_i
        x0      % initial primal point
        y0      % initial dual point
        
        % Number of calls counter:
        ncalls_FGradF = 0   % function F and its gradient
        ncalls_G = 0        % function G
        ncalls_proxG = 0    % proximal operator of G
        ncalls_H            % functions H_i
        ncalls_proxConjH    % proximal operators of H_i*
        ncalls_directL      % direct matrix-vector products
        ncalls_adjointL     % hermitian adjoint matrix-vector products
        
        % Time in calls:
        time_FGradF = 0     % function F and its gradient
        time_G = 0          % function G
        time_proxG = 0      % proximal operator of G
        time_H              % functions H_i
        time_proxConjH      % proximal operators of H_i*
        time_directL        % direct matrix-vector products
        time_adjointL       % hermitian adjoint matrix-vector products
    end
    
    properties (Access = private)
        % Function and operator handles                                                             Of the form:
        FGradF      % handle of F ands its gradient,                                                [val, grad] = FGradF(x)
        G           % handle of the function G,                                                     val = G(x)
        proxG       % handle of the proximal operator of G,                                         x' = proxG(gamma, x)
        H           % cell array of the handles of the functions H_i,                               val = H{i}(y)
        proxConjH   % cell array of handles of the proximal operators of the conjugates of H_i's,   y' = proxConjH{i}(gamma, y)
        directL     % cell array of handles of the direct products of L_i's,                        y = directL{i}(x)
        adjointL    % cell array of handles of the hermitian adjoint products of L_i's,             x = adjointL{i}(y)
    end
    
    %% Public methods
    methods (Access = public)
        
        function self = NonDiffConvexModel(name, FGradF, G, proxG, H, proxConjH, directL, adjointL, x0, y0)
            self.name = name;
            
            if nargin == 10
                self.Initialize(FGradF, G, proxG, H, proxConjH, directL, adjointL, x0, y0);
            elseif nargin > 1
                self.Initialize(FGradF, G, proxG, H, proxConjH, directL, adjointL, x0);
            end
        end
        
        function Initialize(self, FGradF, G, proxG, H, proxConjH, directL, adjointL, x0, y0)
            self.p = numel(x0);
            self.x0 = reshape(x0, self.p, 1);
            
            self.m = numel(H);
            
            if size(directL) ~= size(H)
                error('H and directL should have the same size.');
            end
            self.d = zeros(1, self.m + 1);
            self.d(1) = 1;
            for i = 1 : self.m
                self.d(i + 1) = self.d(i) + numel(directL{i}(zeros(self.p, 1)));
            end
            
            if nargin == 10 && numel(y0) == self.d(end) - 1
                self.y0 = reshape(y0, self.d(end) - 1, 1);
            else
                self.y0 = zeros(self.d(end) - 1, 1);
            end
            
            self.ncalls_H = zeros(1, self.m);
            self.ncalls_proxConjH = zeros(1, self.m);
            self.ncalls_directL = zeros(1, self.m);
            self.ncalls_adjointL = zeros(1, self.m);
            
            self.time_H = zeros(1, self.m);
            self.time_proxConjH = zeros(1, self.m);
            self.time_directL = zeros(1, self.m);
            self.time_adjointL = zeros(1, self.m);
            
            self.FGradF = FGradF;
            self.G = G;
            self.proxG = proxG;
            self.H = reshape(H, 1, self.m);
            
            if size(proxConjH) ~= size(H)
                error('H and proxConjH should have the same size.');
            end
            self.proxConjH = reshape(proxConjH, 1, self.m);
            
            self.directL = reshape(directL, 1, self.m);
            
            if size(adjointL) ~= size(H)
                error('H and adjointL should have the same size.');
            end
            self.adjointL = reshape(adjointL, 1, self.m);
        end
        
        function [F, gradF] = evalFGradF(self, x)
            self.ncalls_FGradF = self.ncalls_FGradF + 1;
            time = tic;
            [F, gradF] = self.FGradF(x);
            self.time_FGradF = self.time_FGradF + toc(time);
        end
        
        function value = evalG(self, x)
            self.ncalls_G = self.ncalls_G + 1;
            time = tic;
            value = self.G(x);
            self.time_G = self.time_G + toc(time);
        end
        
        function value = evalProxG(self, gamma, x)
            self.ncalls_proxG = self.ncalls_proxG + 1;
            time = tic;
            value = self.proxG(gamma, x);
            self.time_proxG = self.time_proxG + toc(time);
        end
        
        function value = evalH_i(self, i, y_i)
            self.ncalls_H(i) = self.ncalls_H(i) + 1;
            time = tic;
            value = self.H{i}(y_i);
            self.time_H(i) = self.time_H(i) + toc(time);
        end
        
        function value = evalProxConjH_i(self, i, gamma, y_i)
            self.ncalls_proxConjH(i) = self.ncalls_proxConjH(i) + 1;
            time = tic;
            value = self.proxConjH{i}(gamma, y_i);
            self.time_proxConjH(i) = self.time_proxConjH(i) + toc(time);
        end
        
        function value = evalDirectL_i(self, i, x)
            self.ncalls_directL(i) = self.ncalls_directL(i) + 1;
            time = tic;
            value = self.directL{i}(x);
            self.time_directL(i) = self.time_directL(i) + toc(time);
        end
        
        function value = evalAdjointL_i(self, i, y_i)
            self.ncalls_adjointL(i) = self.ncalls_adjointL(i) + 1;
            time = tic;
            value = self.adjointL{i}(y_i);
            self.time_adjointL(i) = self.time_adjointL(i) + toc(time);
        end

        function resetCounters(self)
            self.ncalls_FGradF = 0;
            self.ncalls_G = 0;
            self.ncalls_proxG = 0;
            self.ncalls_H = zeros(size(self.H));
            self.ncalls_proxConjH = zeros(size(self.H));
            self.ncalls_directL = zeros(size(self.H));
            self.ncalls_adjointL = zeros(size(self.H));
        
            self.time_FGradF = 0;
            self.time_G = 0;
            self.time_proxG = 0;
            self.time_H = zeros(size(self.H));
            self.time_proxConjH = zeros(size(self.H));
            self.time_directL = zeros(size(self.H));
            self.time_adjointL = zeros(size(self.H));
        end
        
    end
    
end

