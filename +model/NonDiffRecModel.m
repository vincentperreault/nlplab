classdef NonDiffRecModel < model.NonDiffConvexModel
    %% NonDiffRecModel - Model representing the reconstruction problem with non-differentiable constraints
    %   This class was developped to represent the following problem
    %
    %                                m
    %   min [ 1/2 || PCx - y ||^2 + sum( H (L  x) ) ]
    %    x                          i=1   i  i
    %
    %   where P and C are matrices that don't have an explicit form, i.e.
    %   only their matrix-vector product is available and we have
    %
    %   H  = X   ;         L  = C.
    %    1     n            1
    %         R
    %          +
    %
    %   This class is a subclass of NonDiffConvexmodel and can therefore be
    %   passed to solvers requiring such models (Condat primal dual split,
    %   versions A & B; Yan primal dual split; etc.)
    %
    %   This model is only compatible with the structure of the tomographic
    %   reconstruction algorithm made by Poly-LION. The terms in the
    %   objective function should be provided by a Critere object, whereas
    %   the preconditionner should be a Precond object. This model also
    %   expects the sinogram to be a Sinogramme object and the geos a
    %   GeometrieSeries object.
    %
    % NonDiffRecModel(crit, prec, sino, geos, *mu0, *name)
    % where * denotes optional arguments
    
    %% Properties
    properties (Access = private)
        % Storing the object representing the problem
        crit; % Contains terms representing the objective function
        prec; % The preconditionner used, identity = none
        sino; % The sinogram of the problem
        geos; % The geometry that is used
    end
    
    properties (SetAccess = private, Hidden = false)
        % Indices of differentiable critère elements
        iDiffCritElem = [];
    end
    
    %% Public methods
    methods
        
        function self = NonDiffRecModel(name, crit, prec, sino, geos, mu0, y0)
            % Constructor
            % Inputs:
            %   - name: name of the nlp model
            %   - crit: Critere object from the Poly-LION repository
            %   - prec: Precond object from the Poly-Lion repository
            %   - sino: Sinogramme object from the Poly-Lion repository
            %   - geos: Geometrie_Series object from the Poly-Lion repo
            %   - mu0:  initial vector for the reconstruction
            %   problem
            %   - y0: 	initial vector for the dual variables
            
            % Checking the arguments
            if ~isa(crit, 'Critere')
                error('Object crit should be a Critere object');
            elseif ~isa(prec, 'Precond')
                error('Object prec should be a Precond object');
            elseif ~isa(sino, 'Sinogramme')
                error('Object sino should be a Sinogramme object');
            elseif ~isa(geos, 'GeometrieSeries')
                error('Object geos should be a GeometrieSeries object');
            end
            
            % Getting object size depending on coordinates type
            if isa(geos, 'GeoS_Cart')
                objSiz = geos.Rows * geos.Columns * geos.Slices;
            else % Has to be GeoS_Pol
                objSiz = geos.Rhos * geos.Thetas * geos.Slices;
            end
            
            if geos.Slices > 1
                error('NonDiffRecModel is only implemented for 2D.');
            end
            
            % In case of missing/empty arg or size mismatch
            if isempty(mu0)
                mu0 = spalloc(objSiz, 1, objSiz);
            elseif length(mu0(:)) ~= objSiz
                error(['Initial value vector''s size doesn''t match', ...
                    'the object'])
            end
            
            % Calling the NonDiffConvexModel superclass (required for primal-dual splits)
            self = self@model.NonDiffConvexModel(name);
            
            % Collecting the indices of the differentiable terms in the critere
            nAdeq = 0;
            for i = 1 : crit.nElemts
                if isa(crit.J{i}, 'Adequation')
                    nAdeq = nAdeq + 1;
                    self.iDiffCritElem(end+1) = i;
                elseif isa(crit.J{i}, 'PenalObj_L2L1') || isa(crit.J{i}, 'PenalObj_L2') || isa(crit.J{i}, 'PenalGradObj_L2L1') || isa(crit.J{i}, 'PenalGradObj_L2')
                    self.iDiffCritElem(end+1) = i;
                end
            end
            
            % Making sure there is only one Adequation term in the critere
            if nAdeq ~= 1
                error(['There should be one Adequation term in the', ...
                    'critobject']);
            end
            
            % Assigning properties
            self.crit = crit;
            self.prec = prec;
            self.sino = sino;
            self.geos = geos;
            
            % Creating function handles
            % FGradF has its own local function
            FGradF = @(x) self.FGradFRec(x);
            
            % G = 0 in this problem
            G = @(x) 0;
            proxG = @(gamma, x) x;
            
            % Initializing H, proxConjH, directL, adjointL
            H = cell(1, self.crit.nElemts - numel(self.iDiffCritElem) + 1);
            proxConjH = cell(1, self.crit.nElemts - numel(self.iDiffCritElem) + 1);
            directL = cell(1, self.crit.nElemts - numel(self.iDiffCritElem) + 1);
            adjointL = cell(1, self.crit.nElemts - numel(self.iDiffCritElem) + 1);
            
            % The first composite term is the restriction to the domain
            H{1} = @(y) utils.Proximal.RectangleIndicator(zeros(size(y)), Inf(size(y)), y);
            proxConjH{1} = @(gamma, y) utils.Proximal.ProxConjRectangleIndicator(gamma, zeros(size(y)), Inf(size(y)), y);
            directL{1} = @(x) self.prec.Direct(x);
            adjointL{1} = @(y) self.prec.Adjoint(y);
            
            % Filling the other composite terms
            iNonDiffCritElem = setdiff(1 : self.crit.nElemts, self.iDiffCritElem);
            for i = 1:numel(iNonDiffCritElem)
                if isa(self.crit.J{iNonDiffCritElem(i)}, 'PenalObj_L1')
                    H{i+1} = @(y) self.crit.J{iNonDiffCritElem(i)}.lambda * norm(y, 1);
                    proxConjH{i+1} = @(gamma, y) utils.Proximal.ProxConjL1(self.crit.J{iNonDiffCritElem(i)}.lambda, y);
                    directL{i+1} = @(x) self.geos.J_D0() .* self.prec.Direct(x);
                    adjointL{i+1} = @(y) self.prec.Adjoint(self.geos.J_D0() .* y);
                elseif isa(self.crit.J{iNonDiffCritElem(i)}, 'PenalGradObj_L1')
                    H{i+1} = @(y) self.crit.J{iNonDiffCritElem(i)}.lambda * norm(y, 1);
                    proxConjH{i+1} = @(gamma, y) utils.Proximal.ProxConjL1(self.crit.J{iNonDiffCritElem(i)}.lambda, y);
                    directL{i+1} = @(x) [self.geos.J_D1n() .* self.geos.D1n(self.prec.Direct(x)); self.geos.J_D1m() .* self.geos.D1m(self.prec.Direct(x))];
                    adjointL{i+1} = @(y) self.prec.Adjoint(self.geos.D1Tn(self.geos.J_D1n() .* y(1:(self.geos.Thetas*self.geos.Rhos))) + self.geos.D1Tm(self.geos.J_D1m() .* y((self.geos.Thetas*self.geos.Rhos+1):end)));
%                 elseif isa(self.crit.J{iNonDiffCritElem(i)}, 'PenalGradObj_L2')
%                     H{i+1} = @(y) self.crit.J{iNonDiffCritElem(i)}.lambda * norm(y, 2);
%                     proxConjH{i+1} = @(gamma, y) utils.Proximal.ProxConjL2(gamma, self.crit.J{iNonDiffCritElem(i)}.lambda, y);
%                     directL{i+1} = @(x) [sqrt(self.geos.J_D1n()) .* self.geos.D1n(self.prec.Direct(x)); sqrt(self.geos.J_D1m()) .* self.geos.D1m(self.prec.Direct(x))];
%                     adjointL{i+1} = @(y) self.prec.Adjoint(self.geos.D1Tn(sqrt(self.geos.J_D1n()) .* y(1:(self.geos.Thetas*self.geos.Rhos))) + self.geos.D1Tm(sqrt(self.geos.J_D1m()) .* y((self.geos.Thetas*self.geos.Rhos+1):end)));
                end
            end
            
            % Converting to the preconditionned variable
            % mu = Cx <=> x = C^-1 mu
            x0 = self.prec.Inverse(full(mu0));
            
            % Completing initialization of the superclass's properties
            self.Initialize(FGradF, G, proxG, H, proxConjH, directL, adjointL, x0, y0);
        end
        
    end
    
    methods (Access = private)
        
        function [F, gradF] = FGradFRec(self, x)
            F = 0;
            gradF = 0;
            for i = self.iDiffCritElem
                tmp = self.crit.J{i}.val_grad(self.prec.Direct(x), self.sino.Scans{1});
                F = F + tmp.valeur;
                gradF = gradF + tmp.gradient;
            end
            gradF = self.prec.Adjoint(gradF);
        end
        
    end
    
end

